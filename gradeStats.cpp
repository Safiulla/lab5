#include <iostream>
using namespace std;
int main() {
  int  new_grade, grades[100], sum, count = 0;
  float average;
  while(new_grade != -1){
    cout << "Enter grade (or -1 to end): ";
    cin >> new_grade;
    grades[count] = new_grade;
    count++;
  }

  count -= 1;
  for(int i = (count-1); i>=0; i--){
    sum += grades[i];
  }

  average = (sum/count);
  cout << "Average is: " << average << endl;
}
