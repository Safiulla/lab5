#include <iostream>
using namespace std;
int main()
{
    int start, end;
    int sum =0;
    cout << "Enter the starting and ending numbers: ";
    cin >> start >> end;
    
    for (int i=start; i<=end; i++) {
        sum += i;
    }
    
    cout << "Sum from " << start << " to " << end << " is: " << sum << endl;
    return 0;
}
